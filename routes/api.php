<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuratController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\SuratRekomendasiController;

Route::prefix('json')->as('json.')->group(function () {
    Route::prefix('get')->group(function () {
        Route::post('surat_edit', [SuratController::class, 'json_edit_surat'])->name('surat_edit');
        Route::post('perusahaan_edit', [PerusahaanController::class, 'json_edit'])->name('perusahaan_edit');
        Route::post('menimbang_edit', [SuratRekomendasiController::class, 'json_edit_menimbang'])->name('menimbang_edit');
        Route::post('untuk_edit', [SuratRekomendasiController::class, 'json_edit_untuk'])->name('untuk_edit');
    });
});
