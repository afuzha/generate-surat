<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuratController;
use App\Http\Controllers\SuratRekomendasiController;
use App\Http\Controllers\SuratSKRDController;
use App\Http\Controllers\PerusahaanController;
use App\Http\Controllers\WordController;

Route::get('/', [SuratController::class, 'index'])->name('dashboard');

Route::resource('surat', SuratController::class);
// Route::resource('perusahaan', PerusahaanController::class);
Route::get('perusahaan/', [PerusahaanController::class, 'index'])->name('perusahaan.index');
Route::post('perusahaan/store', [PerusahaanController::class, 'store'])->name('perusahaan.store');
Route::get('perusahaan/{id}/edit', [PerusahaanController::class, 'edit'])->name('perusahaan.edit');
Route::delete('perusahaan/{id}', [PerusahaanController::class, 'destroy'])->name('perusahaan.destroy');
Route::post('perusahaan/{id}', [PerusahaanController::class, 'update'])->name('perusahaan.update');

Route::post('surat/rekomendasi/menimbang/store', [SuratRekomendasiController::class, 'store_menimbang'])->name('menimbang.store');
Route::post('surat/rekomendasi/menimbang/{id}', [SuratRekomendasiController::class, 'update_menimbang'])->name('menimbang.update');
Route::delete('surat/rekomendasi/menimbang/{id}', [SuratRekomendasiController::class, 'destroy_menimbang'])->name('menimbang.destroy');

Route::post('surat/rekomendasi/untuk/store', [SuratRekomendasiController::class, 'store_untuk'])->name('untuk.store');
Route::post('surat/rekomendasi/untuk/{id}', [SuratRekomendasiController::class, 'update_untuk'])->name('untuk.update');
Route::delete('surat/rekomendasi/untuk/{id}', [SuratRekomendasiController::class, 'destroy_untuk'])->name('untuk.destroy');

Route::get('surat', [SuratController::class, 'index'])->name('surat.index');
Route::post('surat/tambah', [SuratController::class, 'store'])->name('surat.store');
Route::post('surat/edit/{id}', [SuratController::class, 'update'])->name('surat.update');
Route::post('surat/upload-template', [SuratController::class, 'upload'])->name('upload.template');
Route::delete('surat/rekomendasi/delete/{id}', [SuratController::class, 'destroy_surat'])->name('surat.destroy');
Route::get('surat/get/perusahaan/{id}', [PerusahaanController::class, 'getPerusahaan'])->name('getPerusahaan');

Route::get('surat/rekomendasi', [SuratRekomendasiController::class, 'index'])->name('surat.rekomendasi.index');
Route::get('surat/rekomendasi/{id}/edit', [SuratRekomendasiController::class, 'edit'])->name('surat.rekomendasi.edit');
Route::post('surat/rekomendasi/tambah', [SuratRekomendasiController::class, 'store'])->name('surat.rekomendasi.store');
Route::post('surat/rekomendasi/tambah/{id}', [SuratRekomendasiController::class, 'update'])->name('surat.rekomendasi.update');

Route::get('surat/skrd/{id}/edit', [SuratSKRDController::class, 'edit'])->name('surat.skrd.edit');
Route::post('surat/skrd/tambah', [SuratSKRDController::class, 'store'])->name('surat.skrd.store');
Route::post('surat/skrd/tambah/{id}', [SuratSKRDController::class, 'update'])->name('surat.skrd.update');

Route::get('word/download/rekomendasi/{id}', [WordController::class, 'word_rekomendasi'])->name('download.rekomendasi');
Route::get('word/download/skrd/{id}', [WordController::class, 'word_skrd'])->name('download.skrd');
