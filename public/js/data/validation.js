$(document).on('nifty.ready', function () {
    $('#validation').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            nama: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nama Surat tidak boleh kosong...'
                    }
                }
            },
            jenis: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Jenis harus dipilih...'
                    }
                }
            },
            perusahaan_id: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Perusahaan harus dipilih...'
                    }
                }
            },
            nomor_surat_rekomendasi: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Rekomendasi harus diisi...'
                    }
                }
            },
            nomor_masuk: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            tanggal_masuk: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            tentang: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            nama_pemohon: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            alamat_menara: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            nama_site: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            peruntukan_menara: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            koordinat_menara: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            tinggi_menara: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            nama_penandatangan: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            nip_penandatangan: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nomor Surat Masuk harus diisi...'
                    }
                }
            },
            huruf: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Huruf harus diisi...'
                    }
                }
            },
            isi: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Isi harus diisi...'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    $('#validation_menimbang').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            huruf: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Huruf harus diisi...'
                    }
                }
            },
            isi: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Isi harus diisi...'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });
    $('#validation_untuk').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            huruf: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Huruf harus diisi...'
                    }
                }
            },
            isi: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Isi harus diisi...'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });
});
