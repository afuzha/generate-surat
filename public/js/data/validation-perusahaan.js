$(document).on('nifty.ready', function () {
    $('#validation').bootstrapValidator({
        message: 'This value is not valid',
        fields: {
            nama: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Nama Perusahaan tidak boleh kosong...'
                    }
                }
            },
            alamat1: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Alamat 1 tidak boleh kosong...'
                    }
                }
            },
            alamat2: {
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'Alamat 2 tidak boleh kosong...'
                    }
                }
            },
        }
    }).on('success.field.bv', function (e, data) {
        // $(e.target)  --> The field element
        // data.bv      --> The BootstrapValidator instance
        // data.field   --> The field name
        // data.element --> The field element

        var $parent = data.element.parents('.form-group');

        // Remove the has-success class
        $parent.removeClass('has-success');
    });
});
