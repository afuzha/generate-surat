<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Untuk extends Model
{
    use HasFactory;
    protected $table = 'untuk';
    protected $fillable = [
        'surat_id',
        'huruf',
        'isi',
    ];
}
