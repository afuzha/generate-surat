<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailSuratSKRD extends Model
{
    use HasFactory;
    protected $table = 'd_surat_skrd';
    protected $fillable = [
        'perusahaan_id',
        'nomor_surat_skrd',
        'jenis_menara',
        'jumlah_menara',
        'jumlah_retribusi',
        'site_nama',
        'jarak_tempuh',
        'masa_retribusi_start',
        'masa_retribusi_end',
        'jatuh_tempo',
        'biaya_distribusi',
        'posisi_menara',
        'indeks_lokasi',
        'indeks_jenis',
        'nama_penandatangan',
        'nip_penandatangan',
    ];

    public function perusahaan()
    {
        return $this->belongsTo('App\Models\Perusahaan');
    }
}
