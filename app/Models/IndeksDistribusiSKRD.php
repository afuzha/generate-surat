<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IndeksDistribusiSKRD extends Model
{
    use HasFactory;
    protected $table = 'indeks_distribusi_skrd';
    protected $fillable = [
        'nama',
        'indeks'
    ];
}
