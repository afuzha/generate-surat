<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DetailSuratRekomendasi extends Model
{
    use HasFactory;
    protected $table = 'd_surat_rekomendasi';
    protected $fillable = [
        'perusahaan_id',
        'nomor_surat_rekomendasi',
        'nomor_masuk',
        'tanggal_masuk',
        'tentang',
        'nama_pemohon',
        'alamat_menara',
        'nama_site',
        'peruntukan_menara',
        'koordinat_menara',
        'tinggi_menara',
        'nama_penandatangan',
        'nip_penandatangan',
    ];

    public function perusahaan()
    {
        return $this->belongsTo('App\Models\Perusahaan');
    }
}
