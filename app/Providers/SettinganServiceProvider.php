<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class SettinganServiceProvider extends ServiceProvider
{
    public function register()
    {
        require_once app_path() . '/Helpers/Settingan.php';
    }

    public function boot()
    {
        //
    }
}
