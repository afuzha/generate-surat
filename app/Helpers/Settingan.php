<?php

namespace App\Helpers;

use App\Models\IndeksDistribusiSKRD;

class Settingan
{
  public static function roles()
  {
    $role =  Role::all();
    return $role;
  }
  public static function jenisMenara()
  {
    $indeks = IndeksDistribusiSKRD::all();
    return $indeks;
  }

  public static function lokasiMenara()
  {
    $array = [
      'Dalam Kota',
      'Luar Kota'
    ];
    return $array;
  }

  public static function rupiah($angka)
  {
    $hasil_rupiah = "Rp. " . number_format($angka, 2, ',', '.');
    return $hasil_rupiah;
  }

  public static function jenisSurat()
  {
    $jenis = [
      'Rekomendasi',
      'SKRD'
    ];
    return $jenis;
  }
}
