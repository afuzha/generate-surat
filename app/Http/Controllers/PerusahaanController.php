<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Perusahaan;

class PerusahaanController extends Controller
{
    public function index()
    {
        $datas = Perusahaan::all();
        return view('page.perusahaan.index', compact('datas'));
    }

    public function store(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'alamat1' => 'required',
            'alamat2' => 'required',
        ];
        $customMessages = [
            'required' => ':Attribute tidak boleh kosong!',
        ];
        $this->validate($request, $rules, $customMessages);

        $datas = new Perusahaan;
        $datas->nama = $request->nama;
        $datas->alamat1 = $request->alamat1;
        $datas->alamat2 = $request->alamat2;
        $datas->save();

        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Perusahaan telah ditambahkan...',
            ]);
    }

    public function json_edit(request $request)
    {
        $data = Perusahaan::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function update(Request $request, $id)
    {
        $datas = Perusahaan::findOrFail($id);
        $datas->nama = $request->nama;
        $datas->alamat1 = $request->alamat1;
        $datas->alamat2 = $request->alamat2;
        $datas->save();


        return back()
            ->with([
                'success' => 'Berhasil!!',
                'isi' => 'Perusahaan telah diperbaharui...',
            ]);
    }

    public function destroy($id)
    {
        $datas = Perusahaan::findOrFail($id);
        $datas->delete();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Perusahaan telah dihapus...');
    }

    public function getPerusahaan($id)
    {
        $data = Perusahaan::find($id);
        echo json_encode($data);

        exit;
    }
}
