<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Surat;
use App\Models\DetailSuratRekomendasi;
use App\Models\Perusahaan;
use App\Models\Menimbang;
use App\Models\Untuk;

class SuratRekomendasiController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        // $rules = [
        //     'file' => 'required|mimes:doc,docx,csv,rtf,xlsx,xls,txt,zip',
        // ];

        // $customMessages = [
        //     'nullable' => ':attribute boleh dikosongkan.',
        //     'required' => ':Attribute harus dipilih.',
        //     'mimes' => 'Ekstensi :attribute tidak sesuai!!'
        // ];

        // $this->validate($request, $rules, $customMessages);

        $data = new DetailSuratRekomendasi;
        $data->id = $request->id;
        $data->perusahaan_id = $request->perusahaan_id;
        $data->nomor_surat_rekomendasi = $request->nomor_surat_rekomendasi;
        $data->nomor_masuk = $request->nomor_masuk;
        $data->tanggal_masuk = date('Y-m-d', strtotime($request->tanggal_masuk));
        $data->tentang = $request->tentang;
        $data->nama_pemohon = $request->nama_pemohon;
        $data->alamat_menara = $request->alamat_menara;
        $data->nama_site = $request->nama_site;
        $data->peruntukan_menara = $request->peruntukan_menara;
        $data->koordinat_menara = $request->koordinat_menara;
        $data->tinggi_menara = $request->tinggi_menara;
        $data->nama_penandatangan = $request->nama_penandatangan;
        $data->nip_penandatangan = $request->nip_penandatangan;
        $data->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Data Surat Rekomendasi telah disimpan...');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $menimbang = Menimbang::where('surat_id', $id)->get();
        $untuk = Untuk::where('surat_id', $id)->get();
        $surat = Surat::where('id', $id)->first();
        $dsurat_rekomendasi = DetailSuratRekomendasi::where('id', $id)->with('perusahaan')->first();
        $perusahaan = Perusahaan::orderBy('id', 'desc')->get();

        return view('page.surat.rekomendasi.edit', compact('surat', 'dsurat_rekomendasi', 'perusahaan', 'menimbang', 'untuk'));
    }

    public function update(Request $request, $id)
    {
        $datas = DetailSuratRekomendasi::findOrFail($id);
        $datas->id = $request->id;
        $datas->perusahaan_id = $request->perusahaan_id;
        $datas->nomor_surat_rekomendasi = $request->nomor_surat_rekomendasi;
        $datas->nomor_masuk = $request->nomor_masuk;
        $datas->tanggal_masuk = date('Y-m-d', strtotime($request->tanggal_masuk));
        $datas->tentang = $request->tentang;
        $datas->nama_pemohon = $request->nama_pemohon;
        $datas->alamat_menara = $request->alamat_menara;
        $datas->nama_site = $request->nama_site;
        $datas->peruntukan_menara = $request->peruntukan_menara;
        $datas->koordinat_menara = $request->koordinat_menara;
        $datas->tinggi_menara = $request->tinggi_menara;
        $datas->nama_penandatangan = $request->nama_penandatangan;
        $datas->nip_penandatangan = $request->nip_penandatangan;
        $datas->update();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Data Surat Rekomendasi telah berhasil di perbaharui...');
    }

    public function destroy($id)
    {
        //
    }

    #Menimbang#
    public function store_menimbang(Request $request)
    {
        Menimbang::create($request->all());

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Menimbang telah ditambahkan...');
    }

    public function json_edit_menimbang(Request $request)
    {
        $data = Menimbang::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function update_menimbang(Request $request, $id)
    {
        $menimbang = Menimbang::findOrFail($id);
        $menimbang->surat_id = $request->surat_id;
        $menimbang->huruf = $request->huruf;
        $menimbang->isi = $request->isi;
        $menimbang->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Menimbang telah diperbaharui...');
    }

    public function destroy_menimbang($id)
    {
        $menimbang = Menimbang::findOrFail($id);
        $menimbang->delete();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Menimbang telah dihapus...');
    }
    #Menimbang#

    #Untuk#
    public function store_untuk(Request $request)
    {
        Untuk::create($request->all());

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Untuk telah ditambahkan...');
    }

    public function json_edit_untuk(Request $request)
    {
        $data = Untuk::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function update_untuk(Request $request, $id)
    {
        $menimbang = Untuk::findOrFail($id);
        $menimbang->surat_id = $request->surat_id;
        $menimbang->huruf = $request->huruf;
        $menimbang->isi = $request->isi;
        $menimbang->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Untuk telah diperbaharui...');
    }

    public function destroy_untuk($id)
    {
        $menimbang = Untuk::findOrFail($id);
        $menimbang->delete();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Row Untuk telah dihapus...');
    }
    #Untuk#
}
