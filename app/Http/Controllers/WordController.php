<?php

namespace App\Http\Controllers;

use \PhpOffice\PhpWord\TemplateProcessor;
use App\Models\DetailSuratRekomendasi;
use App\Models\DetailSuratSKRD;
use App\Models\Menimbang;
use App\Models\Untuk;
use App\Helpers\Settingan;
use Carbon\Carbon;
use NumberFormatter;

class WordController extends Controller
{

    // $digit = new NumberFormatter("id", NumberFormatter::SPELLOUT);
    //     dd($digit->format(2999999));


    public function index()
    {
        $menimbang = Menimbang::orderBy('id', 'asc')->where('surat_id', 9)->get();
        $jumlah_menimbang = Menimbang::where('surat_id', 9)->count();
        foreach ($menimbang as $m) {
            $huruf = $m->huruf;
            $isi = $m->isi;
            $hehe = $huruf . "&nbsp; &nbsp; &nbsp;" . $isi;
            echo $hehe;
        }
        return view('word.index', compact('menimbang'));
    }
    public function word_rekomendasi($id)
    {
        $surat = DetailSuratRekomendasi::findOrFail($id);
        $menimbang = Menimbang::where('surat_id', $id)->get();
        $untuk = Untuk::where('surat_id', $id)->get();
        $jumlah_menimbang = Menimbang::where('surat_id', $id)->count();
        $jumlah_untuk = Untuk::where('surat_id', $id)->count();

        $word = new TemplateProcessor('uploads/Template_Rekomendasi.docx');

        $word->setValues([
            'surat_dari' => strtoupper($surat->perusahaan->nama),
            'nomor_surat_rekomendasi' => $surat->nomor_surat_rekomendasi,
            'nomor_masuk' => $surat->nomor_masuk,
            'tanggal_masuk' => Carbon::parse($surat->tanggal_masuk)->locale('id')->isoFormat('d MMMM Y'),
            'tentang' => $surat->tentang,
            'nama_pemohon' => $surat->nama_pemohon,
            'nama_perusahaan' => strtoupper($surat->perusahaan->nama),
            'nama_pemohon' => $surat->nama_pemohon,
            'alamat1' => $surat->perusahaan->alamat1,
            'alamat2' => $surat->perusahaan->alamat2,
            'alamat_menara' => $surat->alamat_menara,
            'nama_site' => $surat->nama_site,
            'peruntukan_menara' => $surat->peruntukan_menara,
            'koordinat_menara' => $surat->koordinat_menara,
            'tinggi_menara' => $surat->tinggi_menara,
            'nama_penandatangan' => $surat->nama_penandatangan,
            'nip_penandatangan' => $surat->nip_penandatangan,
        ]);

        $word->cloneBlock('block_menimbang', $jumlah_menimbang, true, true);
        $word->cloneBlock('block_untuk', $jumlah_untuk, true, true);

        $i = 1;
        foreach ($menimbang as $m) {
            $word->setValue('a#' . $i, $m->huruf);
            $word->setValue('b#' . $i, $m->isi);
            $i++;
        }

        $x = 1;
        foreach ($untuk as $u) {
            $word->setValue('c#' . $x, $u->huruf);
            $word->setValue('d#' . $x, $u->isi);
            $x++;
        }

        $now = Carbon::now()->format('d-m-Y');
        $nama_file = 'uploads/Surat_Rekomendasi_' . $now . '.docx';

        $word->saveAs($nama_file);

        return response()->download($nama_file)->deleteFileAfterSend(true);
    }

    public function word_skrd($id)
    {
        $skrd = DetailSuratSKRD::findOrFail($id);

        $retribusi = strval(str_replace(",", ".", $skrd->indeks_lokasi) * str_replace(",", ".", $skrd->indeks_jenis) * $skrd->biaya_distribusi);
        $huruf = new NumberFormatter("id-id", NumberFormatter::SPELLOUT);

        $word = new TemplateProcessor('uploads/Template_SKRD.docx');

        $word->setValues([
            'nomor_surat_skrd' => $skrd->nomor_surat_skrd,
            'nama_perusahaan' => $skrd->perusahaan->nama,
            'alamat1' => $skrd->perusahaan->alamat1,
            'alamat2' => $skrd->perusahaan->alamat2,
            'jenis_menara' => $skrd->jenis_menara,
            'jumlah_menara' => $skrd->jumlah_menara,
            'huruf_jumlah_menara' => $huruf->format($skrd->jumlah_menara),
            'jumlah_retribusi' => $skrd->jumlah_retribusi,
            'huruf_jumlah_retribusi' => $huruf->format($skrd->jumlah_retribusi),
            'nama_site' => $skrd->nama_site,
            'jarak_tempuh' => $skrd->jarak_tempuh,
            'masa_retribusi_start' => Carbon::parse($skrd->masa_retribusi_start)->locale('id')->isoFormat('d MMMM Y'),
            'masa_retribusi_end' => Carbon::parse($skrd->masa_retribusi_end)->locale('id')->isoFormat('d MMMM Y'),
            'jatuh_tempo' => Carbon::parse($skrd->jatuh_tempo)->locale('id')->isoFormat('MMMM Y'),
            'biaya_distribusi' => Settingan::rupiah($skrd->biaya_distribusi),
            'indeks_lokasi' => $skrd->indeks_lokasi,
            'huruf_rupiah' => ucwords($huruf->format($retribusi)),
            'indeks_jenis' => $skrd->indeks_jenis,
            'retribusi' => Settingan::rupiah($retribusi),
            'nama_penandatangan' => $skrd->nama_penandatangan,
            'nip_penandatangan' => $skrd->nip_penandatangan,
        ]);

        $now = Carbon::now()->format('d-m-Y');
        $nama_file = 'uploads/SKRD_' . $now . '.docx';

        $word->saveAs($nama_file);

        return response()->download($nama_file)->deleteFileAfterSend(true);
    }
}
