<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Surat;
use App\Models\Template;
use Carbon\Carbon;

class SuratController extends Controller
{
    public function index(Request $request)
    {
        $surat = Surat::orderBy('id', 'desc')->get();
        $template = Template::orderBy('id', 'desc')->get();

        $name = 'Rekomendasi';
        $rekomendasi_doc = public_path() . '/uploads/Template_Rekomendasi.doc';
        $rekomendasi_docx = public_path() . '/uploads/Template_Rekomendasi.docx';
        $skrd_doc = public_path() . '/uploads/Template_SKRD.doc';
        $skrd_docx = public_path() . '/uploads/Template_SKRD.docx';

        return view('page.surat.index', compact('surat', 'rekomendasi_doc', 'rekomendasi_docx', 'skrd_doc', 'skrd_docx'));
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $rules = [
            'nama' => 'required',
            'jenis' => 'required',
        ];
        $customMessages = [
            'nullable' => ':Attribute boleh dikosongkan.',
            'required' => ':Attribute tidak boleh kosong!',
        ];
        $this->validate($request, $rules, $customMessages);

        $surat = new Surat;
        $now = Carbon::now();
        $surat->nama = $request->nama;
        $surat->jenis = $request->jenis;
        $surat->tanggal = $now;
        $surat->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Surat berhasil ditambahkan...');
    }


    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        // $rules = [
        //     'nama' => 'required',
        //     'jenis' => 'required',
        // ];
        // $customMessages = [
        //     'nullable' => ':Attribute boleh dikosongkan.',
        //     'required' => ':Attribute tidak boleh kosong!',
        // ];
        // $this->validate($request, $rules, $customMessages);

        $surat = Surat::findOrFail($id);
        $now = Carbon::now();
        $surat->nama = $request->nama;
        $surat->jenis = $request->jenis;
        $surat->tanggal = $now;
        $surat->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Surat berhasil diperbaharui...');
    }

    public function destroy_surat($id)
    {
        $surat = Surat::findOrFail($id);
        $surat->delete();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Surat telah dihapus...');
    }

    public function json_edit_surat(Request $request)
    {
        $data = Surat::where('id', $request->id)->first();
        return response()->json($data);
    }

    public function upload(Request $request)
    {
        $rules = [
            'file' => 'nullable|mimes:doc,docx,csv,rtf,xlsx,xls,txt,zip'
        ];

        $customMessages = [
            'nullable' => ':attribute boleh dikosongkan.',
            'mimes' => 'Ekstensi :attribute tidak sesuai!!'
        ];

        $this->validate($request, $rules, $customMessages);

        $file_rekomendasi = $request->file('file_rekomendasi');
        $file_skrd = $request->file('file_skrd');
        if ($file_rekomendasi == null) {
        } else {
            $ekst = $file_rekomendasi->getClientOriginalExtension();
        };
        if ($file_skrd == null) {
        } else {
            $ekst = $file_skrd->getClientOriginalExtension();
        }
        if ($file_rekomendasi == null) {
        } else {
            $name_rekomendasi = 'Template_Rekomendasi.' . $ekst;
        }
        if ($file_skrd == null) {
        } else {
            $name_skrd = 'Template_SKRD.' . $ekst;
        }
        $tujuan = public_path() . '/uploads/';
        if ($file_rekomendasi == null) {
        } else {
            $file_rekomendasi->move($tujuan, $name_rekomendasi);
        }
        if ($file_skrd == null) {
        } else {
            $file_skrd->move($tujuan, $name_skrd);
        }

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Template telah ditambahkan...');
    }
}
