<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \PhpOffice\PhpWord\TemplateProcessor;
use App\Models\Surat;

class AdminController extends Controller
{
    public function home()
    {
        return view('page.dashboard');
    }

    public function phpword()
    {
        $word = new TemplateProcessor('word/test.docx');
        $word->setValues([
            'nama' => 'afuza imam',
            'alamat' => 'jl. delima no.81a',
        ]);

        $word->saveAs('word/hasil.docx');
        return response()->download('word/hasil.docx')->deleteFileAfterSend(true);
    }
}
