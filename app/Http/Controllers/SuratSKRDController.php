<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Surat;
use App\Models\IndeksDistribusiSKRD;
use App\Models\DetailSuratSKRD;
use App\Models\Perusahaan;
use NumberFormatter;
use Carbon\Carbon;

class SuratSKRDController extends Controller
{
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $data = new DetailSuratSKRD();
        $data->id = $request->id;
        $data->perusahaan_id = $request->perusahaan_id;
        $data->nomor_surat_skrd = $request->nomor_surat_skrd;
        $data->nama_site = $request->nama_site;
        $data->masa_retribusi_start = date('Y-m-d', strtotime($request->masa_retribusi_start));
        $data->masa_retribusi_end = date('Y-m-d', strtotime($request->masa_retribusi_end));
        $data->jatuh_tempo = date('Y-m-d', strtotime($request->jatuh_tempo));
        $data->nama_penandatangan = $request->nama_penandatangan;
        $data->nip_penandatangan = $request->nip_penandatangan;
        $data->jenis_menara = $request->jenis_menara;
        $data->posisi_menara = $request->posisi_menara;
        $data->jumlah_menara = $request->jumlah_menara;
        $data->jumlah_retribusi = $request->jumlah_retribusi;
        $data->biaya_distribusi = $request->biaya_distribusi;
        if ($request->posisi_menara == "Dalam Kota") {
            $data->indeks_lokasi = '0,9';
        } else {
            $data->indeks_lokasi = '1,1';
        }
        if ($request->jenis_menara == "Menara Pole") {
            $data->indeks_jenis = '0,9';
        } elseif ($request->jenis_menara == "Menara 3 Kaki") {
            $data->indeks_jenis = '1';
        } else {
            $data->indeks_jenis = '1,1';
        }
        $data->jarak_tempuh = $request->jarak_tempuh;

        $data->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Data Surat Ketetapan Retribusi Daerah telah disimpan...');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $surat = Surat::where('id', $id)->first();
        $indeks = IndeksDistribusiSKRD::all();
        $perusahaan = Perusahaan::orderBy('id', 'desc')->get();
        $data = DetailSuratSKRD::where('id', $id)->first();

        return view('page.surat.skrd.edit', compact('surat', 'indeks', 'data', 'perusahaan'));
    }

    public function update(Request $request, $id)
    {
        $data = DetailSuratSKRD::findOrFail($id);
        $data->id = $request->id;
        $data->perusahaan_id = $request->perusahaan_id;
        $data->nomor_surat_skrd = $request->nomor_surat_skrd;
        $data->nama_site = $request->nama_site;
        $data->masa_retribusi_start = date('Y-m-d', strtotime($request->masa_retribusi_start));
        $data->masa_retribusi_end = date('Y-m-d', strtotime($request->masa_retribusi_end));
        $data->jatuh_tempo = date('Y-m-d', strtotime($request->jatuh_tempo));
        $data->nama_penandatangan = $request->nama_penandatangan;
        $data->nip_penandatangan = $request->nip_penandatangan;
        $data->jenis_menara = $request->jenis_menara;
        $data->posisi_menara = $request->posisi_menara;
        $data->jumlah_menara = $request->jumlah_menara;
        $data->jumlah_retribusi = $request->jumlah_retribusi;
        $data->biaya_distribusi = $request->biaya_distribusi;
        if ($request->posisi_menara == "Dalam Kota") {
            $data->indeks_lokasi = '0,9';
        } else {
            $data->indeks_lokasi = '1,1';
        }
        if ($request->jenis_menara == "Menara Pole") {
            $data->indeks_jenis = '0,9';
        } elseif ($request->jenis_menara == "Menara 3 Kaki") {
            $data->indeks_jenis = '1';
        } else {
            $data->indeks_jenis = '1,1';
        }
        $data->jarak_tempuh = $request->jarak_tempuh;

        $data->save();

        return back()
            ->with('success', 'Berhasil!')
            ->with('isi', 'Data Surat Ketetapan Retribusi Daerah telah diperbaharui...');
    }

    public function destroy($id)
    {
        //
    }
}
