@if (session('default'))
<div data-notify="container" class="alert alert-dismissable alert-default alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-bell" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("default")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (session('info'))
<div data-notify="container" class="alert alert-dismissable alert-info alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-info" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("info")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (session("success"))
<div data-notify="container" class="alert alert-dismissable alert-success alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-thumbs-up" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("success")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (session("warning"))
<div data-notify="container" class="alert alert-dismissable alert-warning alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-exclamation-triangle" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("warning")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (session("danger"))
<div data-notify="container" class="alert alert-dismissable alert-danger alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-skull-crossbones" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("danger")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (session("error"))
<div data-notify="container" class="alert alert-dismissable alert-danger alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-skull-crossbones" data-notify="icon"></span>
  <div class="alert-text">
    <span data-notify="message">{{ session("error")}}</span>
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif
@if (count($errors) > 0)
<div data-notify="container" class="alert alert-dismissable alert-danger alert-notify animated fadeInDown anotify" role="alert" data-notify-position="top-center">
  <span class="alert-icon fa fa-skull-crossbones" data-notify="icon"></span>
  <div class="alert-text">
    {{-- <span class="alert-title" data-notify="title">Gagal Simpan Data !!</span> --}}
    @foreach ($errors->all() as $error)
    <span data-notify="message">{{ $error }}</span><br />
    @endforeach
  </div>
  <button type="button" class="close bnotify" data-dismiss="alert" aria-label="close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif