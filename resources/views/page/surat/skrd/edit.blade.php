@extends('master.admin')

@section('css')
<link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
<link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<!--DataTables [ OPTIONAL ]-->
<link href="{{asset('plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<!--Page content-->
<!--===================================================-->
<div id="page-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel">
        <div class="panel-body">
          <a href="{{route('surat.index')}}" class="btn btn-primary">Kembali</a>
          <a class="btn bnt-sm btn-success pull-right" href="{{route('download.skrd',$surat->id)}}"><i class="fa fa-download"></i> Download Surat</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    @if($data !== null)
    <form action="{{route('surat.skrd.update',$data->id)}}" method="post" id="validation">
      @else
      <form action="{{route('surat.skrd.store')}}" method="post" id="validation">
        @endif
        <div class="col-lg-6">
          <div class="panel">
            <div class="panel-body form-horizontal form-padding">

              @csrf
              <input name="id" type="text" value="{{$surat->id}}" hidden>
              <h5>Surat Ketetapan Retribusi Daerah</h5>
              <br>
              <!--Text Input-->
              <div class="form-group">
                <label class="col-md-3 control-label">No. SKRD</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" name="nomor_surat_skrd" class="form-control" placeholder="Cth:     / RETR / MENARA / 2020" autocomplete="off">
                  @else
                  <input type="text" name="nomor_surat_skrd" value="{{old('nomor_surat_skrd',$data->nomor_surat_skrd)}}" class="form-control" placeholder="Cth:     / RETR / MENARA / 2020" autocomplete="off">
                  @endif
                </div>
              </div>
              <hr>
              <!-- <h5>Menetapkan</h5>
              <br> -->
              <div class="form-group">
                <label class="col-md-3 control-label" for="select">Nama Perusahaan</label>
                <div class="col-md-9">
                  @if($data == null)
                  <select name="perusahaan_id" id="perusahaan" class="selectpicker">
                    <option data-hidden="true">-- Pilih Perusahaan --</option>
                    @foreach($perusahaan as $p)
                    <option value="{{$p->id}}">{{$p->nama}}</option>
                    @endforeach
                  </select>
                  @else
                  <select name="perusahaan_id" id="perusahaan" class="selectpicker">
                    <option data-hidden="true">-- Pilih Perusahaan --</option>
                    @foreach($perusahaan as $p)
                    <option value="{{$p->id}}" {{ old('perusahaan_id', $data->perusahaan_id) == $p->id ? 'selected' : '' }}>{{$p->nama}}</option>
                    @endforeach
                  </select>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Perusahaan 1</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" id="alamat_perusahaan_1" name="alamat1" value="" class="form-control" placeholder="Alamat Perusahaan Otomatis Terisi setelah pilih Perusahaan." disabled autocomplete="off">
                  @else
                  <input type="text" id="alamat_perusahaan_1" name="alamat1" value="" class="form-control" placeholder="{{$data->perusahaan->alamat1}}" disabled autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Alamat Perusahaan 2</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" id="alamat_perusahaan_2" name="alamat2" value="" class="form-control" placeholder="Alamat Perusahaan Otomatis Terisi setelah pilih Perusahaan." disabled autocomplete="off">
                  @else
                  <input type="text" id="alamat_perusahaan_2" name="alamat2" value="" class="form-control" placeholder="{{$data->perusahaan->alamat2}}" disabled autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Nama Site</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" name="nama_site" class="form-control" placeholder="Nama Site." autocomplete="off">
                  @else
                  <input value="{{old('nama_site',$data->nama_site)}}" type="text" name="nama_site" class="form-control" placeholder="Nama Site." autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Menara</label>
                <div class="col-md-2">
                  @if($data == null)
                  <input type="number" name="jumlah_menara" class="form-control" placeholder="Cth. 1 (satu)" autocomplete="off">
                  @else
                  <input value="{{old('jumlah_menara',$data->jumlah_menara)}}" type="number" name="jumlah_menara" class="form-control" placeholder="Cth. 1 (satu)" autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Jumlah Retribusi</label>
                <div class="col-md-2">
                  @if($data == null)
                  <input type="number" name="jumlah_retribusi" class="form-control" placeholder="Cth. 1 (satu)" autocomplete="off">
                  @else
                  <input value="{{old('jumlah_retribusi',$data->jumlah_retribusi)}}" type="number" name="jumlah_retribusi" class="form-control" placeholder="Cth. 1 (satu)" autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Masa Retribusi</label>
                <div class="col-md-9">
                  <div id="demo-dp-range">
                    <div class="input-daterange input-group" id="datepicker">
                      @if($data == null)
                      <input type="text" class="form-control" name="masa_retribusi_start" placeholder="Tanggal Mulai..." />
                      <span class="input-group-addon">to</span>
                      <input type="text" class="form-control" name="masa_retribusi_end" placeholder="Tanggal Berakhir..." />
                      @else
                      <input value="{{old('masa_retribusi_start',date('d-m-Y', strtotime($data->masa_retribusi_start)))}}" type="text" class="form-control" name="masa_retribusi_start" placeholder="Tanggal Mulai..." />
                      <span class="input-group-addon">to</span>
                      <input value="{{old('masa_retribusi_end',date('d-m-Y', strtotime($data->masa_retribusi_end)))}}" type="text" class="form-control" name="masa_retribusi_end" placeholder="Tanggal Berakhir..." />
                      @endif
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Tanggal Jatuh Tempo</label>
                <div class="col-md-9">
                  <div id="datepicker">
                    <div class="input-group date">
                      @if($data == null)
                      <input type="text" value="" name="jatuh_tempo" class="form-control" placeholder="Pilih Tanggal Surat Masuk..." autocomplete="off">
                      @else
                      <input value="{{old('jatuh_tempo',date('d-m-Y', strtotime($data->jatuh_tempo)))}}" type="text" value="" name="jatuh_tempo" class="form-control" placeholder="Pilih Tanggal Surat Masuk..." autocomplete="off">
                      @endif
                      <span class="input-group-addon"><i class="demo-pli-calendar-4"></i></span>
                    </div>
                  </div>
                </div>
              </div>

              <hr>
              <h5>Penandatangan Surat Ketetapan Retribusi Daerah</h5>
              <br>

              <div class="form-group">
                <label class="col-md-3 control-label">Nama Penandatangan</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" name="nama_penandatangan" class="form-control" placeholder="Cth. Afuza Imam Awed Alfaruq" autocomplete="off">
                  @else
                  <input value="{{old('nama_penandatangan',$data->nama_penandatangan)}}" type="text" name="nama_penandatangan" class="form-control" placeholder="Cth. Afuza Imam Awed Alfaruq" autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">NIP Penandatangan</label>
                <div class="col-md-9">
                  @if($data == null)
                  <input type="text" name="nip_penandatangan" class="form-control" placeholder="Cth. 199301062020121010" autocomplete="off">
                  @else
                  <input value="{{old('nip_penandatangan',$data->nip_penandatangan)}}" type="text" name="nip_penandatangan" class="form-control" placeholder="Cth. 199301062020121010" autocomplete="off">
                  @endif
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-3"></div>
                <div class="col-md-9">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-lg-6">
          <div class="panel">
            <div class="panel-body form-horizontal form-padding">

              <div class="form-group">
                <label class="col-md-3 control-label" for="select">Posisi Menara</label>
                <div class="col-md-9">
                  @if($data == null)
                  <select name="posisi_menara" id="posisi_menara" class="selectpicker">
                    <option data-hidden="true">-- Pilih Posisi Menara --</option>
                    @foreach(Settingan::lokasiMenara() as $a => $i)
                    <option value="{{$i}}">{{$i}}</option>
                    @endforeach
                  </select>
                  @else
                  <select name="posisi_menara" id="posisi_menara" class="selectpicker">
                    <option data-hidden="true">-- Pilih Posisi Menara --</option>
                    @foreach(Settingan::lokasiMenara() as $a => $i)
                    <option value="{{$i}}" {{ old('posisi_menara', $data->posisi_menara) == $i ? 'selected' : '' }}>{{$i}}</option>
                    @endforeach
                  </select>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label" for="select">Jenis Menara</label>
                <div class="col-md-9">
                  @if($data == null)
                  <select name="jenis_menara" id="jenis_menara" class="selectpicker">
                    <option data-hidden="true">-- Pilih Jenis Menara --</option>
                    @foreach($indeks as $i)
                    <option value="{{$i->nama}}">{{$i->nama}}</option>
                    @endforeach
                  </select>
                  @else
                  <select name="jenis_menara" id="jenis_menara" class="selectpicker">
                    <option data-hidden="true">-- Pilih Jenis Menara --</option>
                    @foreach($indeks as $i)
                    <option value="{{$i->nama}}" {{ old('jenis_menara', $data->jenis_menara) == $i->nama ? 'selected' : '' }}>{{$i->nama}}</option>
                    @endforeach
                  </select>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Biaya Distribusi</label>
                <div class="col-md-5">
                  @if($data == null)
                  <input type="text" name="biaya_distribusi" class="form-control" placeholder="Cth. 10000000" autocomplete="off">
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input type="text" name="biaya_distribusi" class="form-control" placeholder="Cth. 10000000" autocomplete="off">
                    <span class="input-group-addon">,00</span>
                  </div>
                  @else
                  <div class="input-group">
                    <span class="input-group-addon">Rp.</span>
                    <input value="{{old('biaya_distribusi',$data->biaya_distribusi)}}" type="text" name="biaya_distribusi" class="form-control" placeholder="Cth. 10000000" autocomplete="off">
                    <span class="input-group-addon">,00</span>
                  </div>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <label class="col-md-3 control-label">Jarak Tempuh</label>
                <div class="col-md-3">
                  @if($data == null)
                  <input type="number" name="jarak_tempuh" class="form-control" placeholder="Cth. 20" autocomplete="off">
                  @else
                  <div class="input-group">
                    <input value="{{old('jarak_tempuh',$data->jarak_tempuh)}}" type="number" name="jarak_tempuh" class="form-control" placeholder="Cth. 20" autocomplete="off">
                    <span class="input-group-addon">Km</span>
                  </div>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <div class="well">
                  <p>
                    <h5 class="title">Rumus Perhitungan Retribusi Pengendalian Menara Telkom</h5>
                  </p>
                  <hr>
                  <p>
                    Indeks Posisi Menara X Indeks Jenis Menara X Biaya yang didistribusikan
                    <hr>
                    @if($data == null)
                    @else
                    {{$data->indeks_lokasi}} x {{$data->indeks_jenis}} x @rupiah($data->biaya_distribusi) = <strong>@rupiah(str_replace(",",".",$data->indeks_lokasi) * str_replace(",",".",$data->indeks_jenis) * $data->biaya_distribusi)</strong>
                    @endif
                  </p>
                  <br>
                  <table class="table">
                    <tr>
                      <th>Variabel</th>
                      <th>Indeks</th>
                    </tr>
                    <tr>
                      <td>Dalam Kota</td>
                      <td>0,9</td>
                    </tr>
                    <tr>
                      <td>Luar Kota</td>
                      <td>1,1</td>
                    </tr>
                    @foreach($indeks as $i)
                    <tr>
                      <td>{{$i->nama}}</td>
                      <td>{{$i->indeks}}</td>
                    </tr>
                    @endforeach
                  </table>
                </div>
              </div>

            </div>
          </div>
        </div>
      </form>
  </div>
</div>
<!--===================================================-->
<!--End page content-->

@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.js')}}"></script>
<script src="{{asset('js/data/validation.js')}}"></script>
<script>
  $(document).on('nifty.ready', function() {
    $('#datepicker .input-group.date').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy',
    });
    // .on('changeDate show', function(e) {
    //   // Revalidate the date field
    //   $('#validation').bootstrapValidator('revalidateField', 'jatuh_tempo');
    // });

  });
</script>
<script>
  $('#demo-dp-range .input-daterange').datepicker({
    format: "dd-mm-yyyy",
    todayBtn: "linked",
    autoclose: true,
    todayHighlight: true
  });
</script>
<script>
  $('#perusahaan').change(function() {
    var id = $(this).val();
    var url = '{{ route("getPerusahaan", ":id") }}';
    url = url.replace(':id', id);

    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function(response) {
        if (response != null) {
          $('#nama_perusahaan').val(response.nama);
          $('#alamat_perusahaan_1').val(response.alamat1);
          $('#alamat_perusahaan_2').val(response.alamat2);
        }
      }
    });
  });
</script>

<script>
  $('.menimbang_table').on('click', '.editMenimbang', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "{{route('surat.rekomendasi.index')}}/" + "menimbang" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.menimbang_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#editMenimbang').modal('show');
        $("#menimbang_huruf").val(data.huruf);
        $("#menimbang_isi").val(data.isi);
        console.log(data);
      }
    });
  });
  $('.untuk_table').on('click', '.editUntuk', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "{{route('surat.rekomendasi.index')}}/" + "untuk" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.untuk_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#editUntuk').modal('show');
        $("#untuk_huruf").val(data.huruf);
        $("#untuk_isi").val(data.isi);
        console.log(data);
      }
    });
  });
</script>

<script>
  $('.hapusMenimbang').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Row Menimbang ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
  $('.hapusUntuk').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Row Untuk ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
</script>

@if(session("success"))
<script>
  $.niftyNoty({
    type: "success",
    container: "floating",
    title: '{{session("success")}}',
    message: '{{session("isi")}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endif
@if($errors->any())
@foreach ($errors->all() as $error)
<script>
  $.niftyNoty({
    type: "danger",
    container: "floating",
    title: 'Gagal!!',
    message: '{{$error}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endforeach
@endif

@endsection