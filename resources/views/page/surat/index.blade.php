@php
use Carbon\Carbon;
@endphp
@extends('master.admin')
<!-- @include('partial/notify') -->

@section('css')
<link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
<!--DataTables [ OPTIONAL ]-->
<link href="{{asset('plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css')}}" rel="stylesheet">
<link href="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.css')}}" rel="stylesheet">
@endsection
@section('content')
<!--Page content-->
<!--===================================================-->
<div id="page-content">

  <div class="panel">
    <div class="panel-body">
      <button class="btn btn-primary pull-right" data-target="#tambah-surat" data-toggle="modal" style="margin-right: 10px;"><i class="fa fa-plus"></i> Tambah Surat</button>
      <button class="btn btn-primary" data-target="#template" data-toggle="modal"><i class="fa fa-file-word-o"></i>&nbsp;&nbsp;Template</button>
    </div>
    <div class="panel-body">
      <table id="dtable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No. Urut</th>
            <th>Nama Surat</th>
            <th>Tanggal Pembuatan</th>
            <th>Jenis Surat</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($surat as $s)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$s->nama}}</td>
            <td>{{Carbon::parse($s->tanggal)->locale('id')->isoFormat('D MMMM Y')}}</td>
            <!-- <td>{{date('d F Y', strtotime($s->tanggal))}}</td> -->
            <td>
              @if($s->jenis=="Rekomendasi")
              <label class="label label-default">{{$s->jenis}}</label>
              @else
              <label class="label label-primary">{{$s->jenis}}</label>
              @endif
            </td>
            <td>
              @if($s->jenis == "Rekomendasi")
              <a href="{{route('surat.rekomendasi.edit',$s->id)}}" class="btn btn-xs btn-primary">Detail</a>
              <button class="btn btn-xs btn-success editNamaSurat" data-id="{{$s->id}}"><i class="fa fa-edit"></i></button>
              @else
              <a href="{{route('surat.skrd.edit',$s->id)}}" class="btn btn-xs btn-primary">Detail</a>
              <button class="btn btn-xs btn-success editNamaSurat" data-id="{{$s->id}}"><i class="fa fa-edit"></i></button>
              @endif
              <form style="display:inline;" method="POST" action="{{route('surat.destroy',$s->id)}}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" class="btn btn-xs btn-danger hapus"><i class="fa fa-trash"></i></button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>
</div>
<!--===================================================-->
<!--End page content-->

<!-- MODAL -->
<div id="tambah-surat" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Tambah Surat</h4>
      </div>
      <div class="modal-body">
        <form id="validation" action="{{route('surat.store')}}" method="post" class="panel-body form-horizontal form-padding">
          @csrf
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="nama-surat">Nama Surat</label>
            <div class="col-md-9">
              <input type="text" name="nama" id="nama-surat" class="form-control" placeholder="Cth. Surat Rekomendasi Telkom Site MT_MAKARTI" autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="select">Jenis Surat</label>
            <div class="col-md-9">
              <select name="jenis" class="selectpicker">
                <option value="" data-hidden="true">-- Pilih Jenis Surat --</option>
                <option value="Rekomendasi">Rekomendasi</option>
                <option value="SKRD">SKRD</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button type="submit" class="btn btn-sm btn-primary">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- MODAL EDIT-->
<div id="edit-surat" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Edit Surat</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="POST" class="panel-body form-horizontal form-padding">
          @csrf
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="nama-surat">Nama Surat</label>
            <div class="col-md-9">
              <input type="text" name="nama" id="edit_nama" class="form-control" autocomplete="off">
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="select">Jenis Surat</label>
            <div class="col-md-9 select">
              <select name="jenis" id="edit_jenis" class="selectpicker">
                <option data-hidden="true"></option>
                @foreach (Settingan::jenisSurat() as $jenis)
                <option value="{{ $jenis }}">{{ $jenis }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button type="submit" class="btn btn-sm btn-primary">Ubah</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL EDIT-->

<div id="template" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">File Template</h4>
      </div>
      <div class="modal-body">
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
          <tr>
            <td>Template Surat Rekomendasi</td>
            @if(is_file($rekomendasi_doc))
            <td><a href="{{url('uploads/Template_Rekomendasi.doc')}}" class="btn btn-xs btn-success">Download Template Surat Rekomendasi</a></td>
            @elseif(is_file($rekomendasi_docx))
            <td><a href="{{url('uploads/Template_Rekomendasi.docx')}}" class="btn btn-xs btn-success">Download Template Surat Rekomendasi</a></td>
            @else
            <td>Belum ada template...</td>
            @endif
          </tr>
          <tr>
            <td>Template SKRD</td>
            @if(is_file($skrd_doc))
            <td><a href="{{url('uploads/Template_SKRD.doc')}}" class="btn btn-xs btn-success">Download Template SKRD</a></td>
            @elseif(is_file($skrd_docx))
            <td><a href="{{url('uploads/Template_Rekomendasi.doc')}}" class="btn btn-xs btn-success">Download Template SKRD</a></td>
            @else
            <td>Belum ada template...</td>
            @endif
          </tr>
        </table>
      </div>
      <div class="modal-header">
        <h4 class="modal-title" id="modal-title">Upload Template</h4>
      </div>
      <form action="{{route('upload.template')}}" method="post" class="panel-body form-horizontal form-padding" enctype="multipart/form-data">
        @csrf
        <table class="table table-striped table-bordered" cellspacing="0" width="100%">
          <tr>
            <td>Surat Rekomendasi</td>
            <td><span class="pull-left btn btn-xs btn-primary btn-file">
                Pilih Template Surat Rekomendasi... <input type="file" name="file_rekomendasi">
              </span></td>
          </tr>
          <tr>
            <td>SKRD</td>
            <td><span class="pull-left btn btn-xs btn-primary btn-file">
                Pilih Template SKRD... <input type="file" name="file_skrd">
              </span></td>
          </tr>
        </table>
        <small class="help-block" style="margin-top:-15px;">&nbsp;&nbsp;&nbsp;Abaikan upload template apabila tidak ingin ubah Template</small>
        <div class="form-group">
          <div class="col-md-5"></div>
          <div class="col-m7-8">
            <button type="submit" class="btn btn-sm btn-primary pull-right">Tambah / Ubah</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.js')}}"></script>
<script src="{{asset('js/data/validation.js')}}"></script>


<script>
  $(document).on('nifty.ready', function() {
    $('#dtable').dataTable({
      "responsive": true,
      "language": {
        "paginate": {
          "previous": '<i class="demo-psi-arrow-left"></i>',
          "next": '<i class="demo-psi-arrow-right"></i>'
        }
      }
    });
  });
</script>

<script>
  $('#dtable').on('click', '.editNamaSurat', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "{{route('surat.index')}}/" + "edit" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.surat_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#edit-surat').modal('show');
        $("#edit_nama").val(data.nama);
        $("#edit_jenis").val(data.jenis).change();
        console.log(data);
      }
    });
  });
</script>

@if(session("success"))
<script>
  $.niftyNoty({
    type: "success",
    container: "floating",
    title: '{{session("success")}}',
    message: '{{session("isi")}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endif
@if($errors->any())
@foreach ($errors->all() as $error)
<script>
  $.niftyNoty({
    type: "danger",
    container: "floating",
    title: 'Gagal!!',
    message: '{{$error}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endforeach
@endif

<script>
  $('.hapus').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Surat ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
</script>

@endsection