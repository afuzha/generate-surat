@extends('master.admin')

@section('css')
<link href="{{asset('plugins/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet">
<link href="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet">
<!--DataTables [ OPTIONAL ]-->
<link href="{{asset('plugins/datatables/media/css/dataTables.bootstrap.css')}}" rel="stylesheet">
<link href="{{asset('plugins/datatables/extensions/Responsive/css/responsive.dataTables.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<!--Page content-->
<!--===================================================-->
<div id="page-content">
  <div class="row">
    <div class="col-lg-12">
      <div class="panel">
        <div class="panel-body">
          <a href="{{route('surat.index')}}" class="btn btn-primary">Kembali</a>
          <a class="btn bnt-sm btn-success pull-right" href="{{route('download.rekomendasi',$surat->id)}}"><i class="fa fa-download"></i> Download Surat</a>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-lg-6">
      <div class="panel">

        <!-- BASIC FORM ELEMENTS -->
        <!--===================================================-->
        @if($dsurat_rekomendasi !== null)
        <form action="{{route('surat.rekomendasi.update',$dsurat_rekomendasi->id)}}" method="post" id="validation" class="panel-body form-horizontal form-padding">
          @else
          <form action="{{route('surat.rekomendasi.store')}}" method="post" id="validation" class="panel-body form-horizontal form-padding">
            @endif

            @csrf
            <input name="id" type="text" value="{{$surat->id}}" hidden>
            <h5>Surat Rekomendasi</h5>
            <br>
            <!--Text Input-->
            <div class="form-group">
              <label class="col-md-3 control-label">No. Surat Rekomendasi</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nomor_surat_rekomendasi" class="form-control" placeholder="Cth: No. 555 / DKISP-I / II / 2021" autocomplete="off">
                @else
                <input type="text" name="nomor_surat_rekomendasi" value="{{old('nomor_surat_rekomendasi',$dsurat_rekomendasi->nomor_surat_rekomendasi)}}" class="form-control" placeholder="Cth: No. 555 / DKISP-I / II / 2021" autocomplete="off">
                @endif
              </div>
            </div>
            <hr>
            <h5>Membaca dan Memperhatikan</h5>
            <br>
            <div class="form-group">
              <label class="col-md-3 control-label" for="select">Surat Dari</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <select name="perusahaan_id" id="perusahaan" class="selectpicker">
                  <option data-hidden="true">-- Pilih Perusahaan --</option>
                  @foreach($perusahaan as $p)
                  <option value="{{$p->id}}">{{$p->nama}}</option>
                  @endforeach
                </select>
                @else
                <select name="perusahaan_id" id="perusahaan" class="selectpicker">
                  <option data-hidden="true">-- Pilih Perusahaan --</option>
                  @foreach($perusahaan as $p)
                  <option value="{{$p->id}}" {{ old('perusahaan_id', $dsurat_rekomendasi->perusahaan_id) == $p->id ? 'selected' : '' }}>{{$p->nama}}</option>
                  @endforeach
                </select>
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Nomor Surat</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nomor_masuk" class="form-control" placeholder="Cth. DMT.0019/DV1/ROE-d1000000/II/2021" autocomplete="off">
                @else
                <input value="{{old('nomor_masuk',$dsurat_rekomendasi->nomor_masuk)}}" type="text" name="nomor_masuk" class="form-control" placeholder="Cth. DMT.0019/DV1/ROE-d1000000/II/2021" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Tanggal</label>
              <div class="col-md-9">
                <div id="datepicker">
                  <div class="input-group date">
                    @if($dsurat_rekomendasi == null)
                    <input type="text" value="" name="tanggal_masuk" class="form-control" placeholder="Pilih Tanggal Surat Masuk..." autocomplete="off">
                    @else
                    <input value="{{old('tanggal_masuk',$dsurat_rekomendasi->tanggal_masuk)}}" type="text" value="" name="tanggal_masuk" class="form-control" placeholder="Pilih Tanggal Surat Masuk..." autocomplete="off">
                    @endif
                    <span class="input-group-addon"><i class="demo-pli-calendar-4"></i></span>
                  </div>
                </div>
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Tentang</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="tentang" class="form-control" placeholder="Tentang." autocomplete="off">
                @else
                <input value="{{old('tentang',$dsurat_rekomendasi->tentang)}}" type="text" name="tentang" class="form-control" placeholder="Tentang." autocomplete="off">
                @endif
              </div>
            </div>

            <hr>

            <h5>Kepada</h5>
            <br>

            <div class="form-group">
              <label class="col-md-3 control-label">Nama Pemohon</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nama_pemohon" class="form-control" placeholder="Cth. Ir. Iwan Adji Purdianto" autocomplete="off">
                @else
                <input value="{{old('nama_pemohon',$dsurat_rekomendasi->nama_pemohon)}}" type="text" name="nama_pemohon" class="form-control" placeholder="Cth. Ir. Iwan Adji Purdianto" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Nama Perusahaan</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null )
                <input type="text" id="nama_perusahaan" class="form-control" placeholder="Nama Perusahaan Otomatis Terisi setelah pilih Perusahaan." disabled autocomplete="off">
                @else
                <input type="text" id="nama_perusahaan" class="form-control" placeholder="{{$dsurat_rekomendasi->perusahaan->nama}}" disabled autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Perusahaan 1</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" id="alamat_perusahaan_1" name="alamat1" value="" class="form-control" placeholder="Alamat Perusahaan Otomatis Terisi setelah pilih Perusahaan." disabled autocomplete="off">
                @else
                <input type="text" id="alamat_perusahaan_1" name="alamat1" value="" class="form-control" placeholder="{{$dsurat_rekomendasi->perusahaan->alamat1}}" disabled autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Perusahaan 2</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" id="alamat_perusahaan_2" name="alamat2" value="" class="form-control" placeholder="Alamat Perusahaan Otomatis Terisi setelah pilih Perusahaan." disabled autocomplete="off">
                @else
                <input type="text" id="alamat_perusahaan_2" name="alamat2" value="" class="form-control" placeholder="{{$dsurat_rekomendasi->perusahaan->alamat2}}" disabled autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Alamat Menara</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="alamat_menara" class="form-control" placeholder="Jl. xxx" autocomplete="off">
                @else
                <input value="{{old('alamat_menara',$dsurat_rekomendasi->alamat_menara)}}" type="text" name="alamat_menara" class="form-control" placeholder="Jl. xxx" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Nama Site</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nama_site" class="form-control" placeholder="Nama Site." autocomplete="off">
                @else
                <input value="{{old('nama_site',$dsurat_rekomendasi->nama_site)}}" type="text" name="nama_site" class="form-control" placeholder="Nama Site." autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Peruntukan Menara</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="peruntukan_menara" value="Menara Telekomunikais (BTS) Multi Operator" class="form-control" placeholder="Peruntukan Menara" autocomplete="off">
                @else
                <input value="{{old('peruntukan_menara',$dsurat_rekomendasi->peruntukan_menara)}}" type="text" name="peruntukan_menara" class="form-control" placeholder="Peruntukan Menara" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Koordinat Menara</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="koordinat_menara" class="form-control" placeholder="Titik Koordinat Menara" autocomplete="off">
                @else
                <input value="{{old('koordinat_menara',$dsurat_rekomendasi->koordinat_menara)}}" type="text" name="koordinat_menara" class="form-control" placeholder="Titik Koordinat Menara" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">Tinggi Menara</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="tinggi_menara" class="form-control" placeholder="Cth. 21 M" autocomplete="off">
                @else
                <input value="{{old('tinggi_menara',$dsurat_rekomendasi->tinggi_menara)}}" type="text" name="tinggi_menara" class="form-control" placeholder="Cth. 21 M" autocomplete="off">
                @endif
              </div>
            </div>

            <hr>
            <h5>Penandatangan Surat Rekomendasi</h5>
            <br>

            <div class="form-group">
              <label class="col-md-3 control-label">Nama Penandatangan</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nama_penandatangan" class="form-control" placeholder="Cth. Afuza Imam Awed Alfaruq" autocomplete="off">
                @else
                <input value="{{old('nama_penandatangan',$dsurat_rekomendasi->nama_penandatangan)}}" type="text" name="nama_penandatangan" class="form-control" placeholder="Cth. Afuza Imam Awed Alfaruq" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <label class="col-md-3 control-label">NIP Penandatangan</label>
              <div class="col-md-9">
                @if($dsurat_rekomendasi == null)
                <input type="text" name="nip_penandatangan" class="form-control" placeholder="Cth. 199301062020121010" autocomplete="off">
                @else
                <input value="{{old('nip_penandatangan',$dsurat_rekomendasi->nip_penandatangan)}}" type="text" name="nip_penandatangan" class="form-control" placeholder="Cth. 199301062020121010" autocomplete="off">
                @endif
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-3"></div>
              <div class="col-md-9">
                <button type="submit" class="btn btn-primary">Simpan</button>
              </div>
            </div>
          </form>
          <!--===================================================-->
          <!-- END BASIC FORM ELEMENTS -->


      </div>
    </div>
    <div class="col-lg-6">
      <div class="tab-base">

        <!--Nav Tabs-->
        <ul class="nav nav-tabs">
          <li class="active">
            <a data-toggle="tab" href="#demo-lft-tab-1">Menimbang</a>
          </li>
          <li>
            <a data-toggle="tab" href="#demo-lft-tab-2">Untuk</a>
          </li>
        </ul>

        <!--Tabs Content-->
        <div class="tab-content">
          <div id="demo-lft-tab-1" class="tab-pane fade active in">
            <div class="table-responsive">
              <table class="table table-bordered menimbang_table">
                <thead>
                  <tr>
                    <th style="width:5%;" class="text-center">Urutan</th>
                    <th style="width:5%;" class="text-center">Huruf</th>
                    <th style="width:75%;" class="text-center">Isi</th>
                    <th style="width:15%;" class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($menimbang as $m)
                  <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td class="text-center">{{$m->huruf}}</td>
                    <td>{{$m->isi}}</td>
                    <td>
                      <button class="btn btn-default btn-xs btn-hover-warning editMenimbang" data-id="{{$m->id}}"><i class="fa fa-edit"></i></button>
                      <form style="display:inline" method="POST" action="{{route('menimbang.destroy',$m->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-default btn-xs btn-hover-danger hapusMenimbang"><i class="fa fa-trash"></i></button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <button class="btn btn-primary pull-right" data-target="#tambah_menimbang" data-toggle="modal">Tambah Row Menimbang</button>
            </div>
          </div>
          <div id="demo-lft-tab-2" class="tab-pane fade">
            <div class="table-responsive">
              <table class="table table-bordered untuk_table">
                <thead>
                  <tr>
                    <th style="width:5%;" class="text-center">Urutan</th>
                    <th style="width:5%;" class="text-left">Huruf</th>
                    <th style="width:74%;" class="text-center">Isi</th>
                    <th style="width:16%;" class="text-center">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($untuk as $u)
                  <tr>
                    <td class="text-center">{{$loop->iteration}}</td>
                    <td class="text-left">{{$u->huruf}}</td>
                    <td>{{$u->isi}}</td>
                    <td>
                      <button class="btn btn-default btn-xs btn-hover-warning editUntuk" data-id="{{$u->id}}"><i class="fa fa-edit"></i></button>
                      <form style="display:inline" method="POST" action="{{route('untuk.destroy',$u->id)}}">
                        {{ csrf_field() }}
                        {{ method_field('DELETE') }}
                        <button type="submit" class="btn btn-default btn-xs btn-hover-danger hapusUntuk"><i class="fa fa-trash"></i></button>
                      </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
              <button class="btn btn-primary pull-right" data-target="#tambah_untuk" data-toggle="modal">Tambah Row Untuk</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<!--===================================================-->
<!--End page content-->


<!-- MODAL TAMBAH MENIMBANG-->
<div id="tambah_menimbang" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Tambah Menimbang</h4>
      </div>
      <div class="modal-body">
        <form id="validation_menimbang" action="{{route('menimbang.store')}}" method="post" class="panel-body form-horizontal form-padding">
          @csrf
          <input type="text" name="surat_id" value="{{$surat->id}}" hidden>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="huruf">Huruf</label>
            <div class="col-md-10">
              <input id="huruf" type="text" name="huruf" class="form-control" placeholder="Cth. a. / 1." autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="isi">Isi</label>
            <div class="col-md-10">
              <textarea id="isi" name="isi" cols="30" rows="3" class="form-control" placeholder="Cth. Menyelenggarakan menara telekomunikasi sesuai dengan ketentuan yang berlaku." autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-10">
              <button type="submit" class="btn btn-sm btn-primary">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL TAMBAH MENIMBANG-->

<!-- MODAL EDIT MENIMBANG -->
<div id="editMenimbang" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Edit Menimbang</h4>
      </div>
      <div class="modal-body">
        <form id="validation" action="#" method="post" class="panel-body form-horizontal form-padding">
          @csrf
          <input type="text" name="surat_id" value="{{$surat->id}}" hidden>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="huruf">Huruf</label>
            <div class="col-md-10">
              <input type="text" name="huruf" id="menimbang_huruf" class="form-control" placeholder="Cth. Pertama, Kedua, Ketiga, dst." autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="isi">Isi</label>
            <div class="col-md-10">
              <textarea name="isi" id="menimbang_isi" cols="30" rows="3" class="form-control" placeholder="Cth. Menyelenggarakan menara telekomunikasi sesuai dengan ketentuan yang berlaku." autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-10">
              <button type="submit" class="btn btn-sm btn-primary">Ubah</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL EDIT MENIMBANG -->

<!-- MODAL Tambah Untuk-->
<div id="tambah_untuk" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Tambah Untuk</h4>
      </div>
      <div class="modal-body">
        <form id="validation_untuk" action="{{route('untuk.store')}}" method="post" class="panel-body form-horizontal form-padding">
          @csrf
          <input type="text" name="surat_id" value="{{$surat->id}}" hidden>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="huruf">Huruf</label>
            <div class="col-md-10">
              <input id="huruf" type="text" name="huruf" class="form-control" placeholder="Cth. Pertama, Kedua" autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="isi">Isi</label>
            <div class="col-md-10">
              <textarea id="isi" name="isi" cols="30" rows="3" class="form-control" placeholder="Cth. Menyelenggarakan menara telekomunikasi sesuai dengan ketentuan yang berlaku." autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-10">
              <button type="submit" class="btn btn-sm btn-primary">Tambahkan</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL Tambah Untuk-->
<!-- MODAL EDIT UNTUK -->
<div id="editUntuk" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Edit Untuk</h4>
      </div>
      <div class="modal-body">
        <form id="validation" action="#" method="post" class="panel-body form-horizontal form-padding">
          @csrf
          <input type="text" name="surat_id" value="{{$surat->id}}" hidden>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="huruf">Huruf</label>
            <div class="col-md-10">
              <input type="text" name="huruf" id="untuk_huruf" class="form-control" placeholder="Cth. a., b., c. dst." autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-2 control-label text-left" for="isi">Isi</label>
            <div class="col-md-10">
              <textarea name="isi" id="untuk_isi" cols="30" rows="3" class="form-control" placeholder="Cth. Menyelenggarakan menara telekomunikasi sesuai dengan ketentuan yang berlaku." autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-2"></div>
            <div class="col-md-10">
              <button type="submit" class="btn btn-sm btn-primary">Ubah</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<!-- MODAL EDIT UNTUK -->

@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-select/bootstrap-select.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-datepicker/bootstrap-datepicker.min.js')}}"></script>
<!--DataTables [ OPTIONAL ]-->
<script src="{{asset('plugins/datatables/media/js/jquery.dataTables.js')}}"></script>
<script src="{{asset('plugins/datatables/media/js/dataTables.bootstrap.js')}}"></script>
<script src="{{asset('plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js')}}"></script>
<script src="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.js')}}"></script>
<script src="{{asset('js/data/validation.js')}}"></script>
<script>
  $(document).on('nifty.ready', function() {
    $('#datepicker .input-group.date').datepicker({
        autoclose: true,
        format: 'dd-mm-yyyy',
      })
      .on('changeDate show', function(e) {
        // Revalidate the date field
        $('#validation').bootstrapValidator('revalidateField', 'tanggal_masuk');
      });

  });
</script>
<script>
  $('#perusahaan').change(function() {
    var id = $(this).val();
    var url = '{{ route("getPerusahaan", ":id") }}';
    url = url.replace(':id', id);

    $.ajax({
      url: url,
      type: 'get',
      dataType: 'json',
      success: function(response) {
        if (response != null) {
          $('#nama_perusahaan').val(response.nama);
          $('#alamat_perusahaan_1').val(response.alamat1);
          $('#alamat_perusahaan_2').val(response.alamat2);
        }
      }
    });
  });
</script>

<script>
  $('.menimbang_table').on('click', '.editMenimbang', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "{{route('surat.rekomendasi.index')}}/" + "menimbang" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.menimbang_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#editMenimbang').modal('show');
        $("#menimbang_huruf").val(data.huruf);
        $("#menimbang_isi").val(data.isi);
        console.log(data);
      }
    });
  });
  $('.untuk_table').on('click', '.editUntuk', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "{{route('surat.rekomendasi.index')}}/" + "untuk" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.untuk_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#editUntuk').modal('show');
        $("#untuk_huruf").val(data.huruf);
        $("#untuk_isi").val(data.isi);
        console.log(data);
      }
    });
  });
</script>

<script>
  $('.hapusMenimbang').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Row Menimbang ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
  $('.hapusUntuk').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Row Untuk ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
</script>

@if(session("success"))
<script>
  $.niftyNoty({
    type: "success",
    container: "floating",
    title: '{{session("success")}}',
    message: '{{session("isi")}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endif
@if($errors->any())
@foreach ($errors->all() as $error)
<script>
  $.niftyNoty({
    type: "danger",
    container: "floating",
    title: 'Gagal!!',
    message: '{{$error}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endforeach
@endif

@endsection