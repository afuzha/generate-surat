@extends('master.admin')

@section('css')
<link href="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.css')}}" rel="stylesheet">
@endsection

@section('content')
<div id="page-content">
  <div class="panel">
    <div class="panel-body">
      <button class="btn btn-primary pull-right" data-target="#tambah_perusahaan" data-toggle="modal" style="margin-right: 10px;"><i class="fa fa-plus"></i> Tambah Perusahaan</button>
    </div>
    <div class="panel-body">
      <table id="dtable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
          <tr>
            <th>No.</th>
            <th>Nama Perusahaan</th>
            <th>Alamat 1</th>
            <th>Alamat 2</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          @foreach($datas as $data)
          <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$data->nama}}</td>
            <td>{{$data->alamat1}}</td>
            <td>{{$data->alamat2}}</td>
            <td>
              <button class="btn btn-xs btn-success editPerusahaan" data-id="{{$data->id}}">Ubah</button>
              <!-- <a class="btn btn-xs btn-success edit" data-id="{{$data->id}}">Ubah</a> -->
              <form style="display:inline;" method="POST" action="{{route('perusahaan.destroy',$data->id)}}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}
                <button type="submit" class="btn btn-xs btn-danger hapus"><i class="fa fa-trash"></i></button>
              </form>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </div>

</div>
<!--===================================================-->
<!--End page content-->

<!-- MODAL TAMBAH -->
<div id="tambah_perusahaan" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Tambah Perusaahaan</h4>
      </div>
      <div class="modal-body">
        @if($datas !== null)
        <form id="validation" action="{{route('perusahaan.store')}}" method="post" class="panel-body form-horizontal form-padding">
          @else
          <form id="validation" action="{{route('perusahaan.store')}}" method="post" class="panel-body form-horizontal form-padding">
            @endif
            @csrf
            <div class="form-group">
              <label class="col-md-3 control-label text-left" for="nama-surat">Nama Perusahaan</label>
              <div class="col-md-9">
                <input id="nama" type="text" name="nama" class="form-control" placeholder="Cth. Surat Rekomendasi Telkom Site MT_MAKARTI" autocomplete="off">
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label text-left" for="alamat1">Alamat 1</label>
              <div class="col-md-9">
                <textarea id="alamat1" class="form-control" name="alamat1" cols="30" rows="3" autocomplete="off"></textarea>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-3 control-label text-left" for="alamat2">Alamat 2</label>
              <div class="col-md-9">
                <textarea id="alamat2" class="form-control" name="alamat2" cols="30" rows="3" autocomplete="off"></textarea>
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-3"></div>
              <div class="col-md-9">
                <button type="submit" class="btn btn-sm btn-primary">Tambahkan</button>
              </div>
            </div>
          </form>
      </div>
    </div>
  </div>
</div>


<!-- MODAL EDIT -->
<div id="editPerusahaan" class="modal fade" tabindex="-1">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><i class="pci-cross pci-circle"></i></button>
        <h4 class="modal-title" id="modal-title">Ubah Perusaahaan</h4>
      </div>
      <div class="modal-body">
        <form id="validation" method="POST" action="#" class="panel-body form-horizontal form-padding">
          @csrf
          <!-- <input id="methodbarang" name="_method" type="hidden" value="PUT"> -->
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="nama-surat">Nama Perusahaan</label>
            <div class="col-md-9">
              <input type="text" value="" name="nama" id="edit_nama" class="form-control" placeholder="Cth. Surat Rekomendasi Telkom Site MT_MAKARTI" autocomplete="off">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="nama-surat">Alamat 1</label>
            <div class="col-md-9">
              <textarea id="edit_alamat1" class="form-control" value="" name="alamat1" cols="30" rows="3" autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label text-left" for="nama-surat">Alamat 2</label>
            <div class="col-md-9">
              <textarea id="edit_alamat2" class="form-control" value="" name="alamat2" cols="30" rows="3" autocomplete="off"></textarea>
            </div>
          </div>
          <div class="form-group">
            <div class="col-md-3"></div>
            <div class="col-md-9">
              <button type="submit" id="tambahkan" class="btn btn-sm btn-primary">Ubah</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script src="{{asset('plugins/bootstrap-validator/bootstrapValidator.min.js')}}"></script>
<script src="{{asset('js/data/validation-perusahaan.js')}}"></script>

<script>
  $('#dtable').on('click', '.editPerusahaan', function() {
    var $id = $(this).attr('data-id');
    $(".modal-body form").attr("action", "perusahaan" + "/" + $id);
    $.ajax({
      type: "POST",
      url: "{{ route('json.perusahaan_edit') }}",
      data: {
        token: '{{ csrf_token() }}',
        id: $id
      },
      dataType: "JSON",
      success: function(data) {
        $('#editPerusahaan').modal('show');
        $("#edit_nama").val(data.nama);
        $("#edit_alamat1").val(data.alamat1);
        $("#edit_alamat2").val(data.alamat2);
        console.log(data);
      }
    });
  });
</script>

@if(session("success"))
<script>
  $.niftyNoty({
    type: "success",
    container: "floating",
    title: '{{session("success")}}',
    message: '{{session("isi")}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endif
@if($errors->any())
@foreach ($errors->all() as $error)
<script>
  $.niftyNoty({
    type: "danger",
    container: "floating",
    title: 'Gagal!!',
    message: '{{$error}}',
    closeBtn: false,
    timer: 4000
  });
</script>
@endforeach
@endif

<script>
  $('.hapus').click(function(e) {
    e.preventDefault() // Don't post the form, unless confirmed
    if (confirm('Apakah anda yakin ingin menghapus Surat ini?')) {
      // Post the form
      $(e.target).closest('form').submit() // Post the surrounding form
    }
  });
</script>
@endsection