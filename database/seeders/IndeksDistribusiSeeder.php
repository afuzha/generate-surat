<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class IndeksDistribusiSeeder extends Seeder
{
    public function run()
    {
        DB::table('indeks_distribusi_skrd')->insert([
            [
                'nama' => 'Menara Pole',
                'indeks' => '0,9'
            ],
            [
                'nama' => 'Menara 3 Kaki',
                'indeks' => '1'
            ],
            [
                'nama' => 'Menara 4 Kaki',
                'indeks' => '1,1'
            ]
        ]);
    }
}
