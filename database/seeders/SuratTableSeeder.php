<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class SuratTableSeeder extends Seeder
{
    public function run()
    {
        // $faker = Faker::create();
        // foreach (range(0, 10) as $i) {
        //     DB::table('surat')->insert([
        //         'nama' => $faker->name,
        //         'tanggal' => $faker->dateTimeThisCentury()->format('Y-m-d H:i:s'),
        //         'jenis' => $faker->randomElement(['Rekomendasi', 'SKRD']),
        //     ]);
        // }

        DB::table('surat')->insert([
            [
                'id' => '1',
                'nama' => 'Surat Rekomendasi Indosat',
                'jenis' => 'Rekomendasi',
                'tanggal' => '2020-10-20'
            ],
            [
                'id' => '2',
                'nama' => 'Surat Ketetapan Retribusi Daerah Telkomsel',
                'jenis' => 'SKRD',
                'tanggal' => '2020-2-20'
            ]
        ]);
    }
}
