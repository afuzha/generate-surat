<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class UntukSeeder extends Seeder
{
    public function run()
    {
        DB::table('untuk')->insert([
            [
                'surat_id' => '1',
                'huruf' => 'Pertama',
                'isi' => 'Menyelenggarakan menara telekomunikasi sesuai dengan ketentuan yang berlaku.'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'Kedua',
                'isi' => 'Melengkapi semua kelengkapan persyaratan dalam penyelenggaraan telekomunikasi sesuai dengan peraturan perundang-undangan yang berlaku dan peraturan lainnya yang dikeluarkan pemerintah.'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'Ketiga',
                'isi' => 'Menyampaikan laporan kepada Dinas Komunikasi Informatika, Statistik dan Persandian Kabupaten Kotawaringin Barat apabila terjadi perubahan penanggung jawab, perubahan bentuk dan penambahan perangkat di menara.'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'Keempat',
                'isi' => 'Membayar kewajiban Retribusi Pengendalian Menara Telekomunikasi kepada Pemerintah Daerah sesuai ketentuan yang berlaku.'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'Kelima',
                'isi' => 'Rekomendasi ini berlaku selama 1 (satu) tahun sejak tanggal dikeluarkan.'
            ]
        ]);
    }
}
