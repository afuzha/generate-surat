<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DataSKRDSeeder extends Seeder
{
    public function run()
    {
        DB::table('d_surat_skrd')->insert([
            'id' => '2',
            'perusahaan_id' => '1',
            'nomor_surat_skrd' => '     / RETR / MENARA / 2020',
            'jenis_menara' => 'Menara Pole',
            'jumlah_menara' => '1',
            'posisi_menara' => 'Luar Kota',
            'jumlah_retribusi' => '1',
            'nama_site' => 'Delima_Empire',
            'jarak_tempuh' => '21',
            'masa_retribusi_start' => '2020-01-01',
            'masa_retribusi_end' => '2020-01-02',
            'jatuh_tempo' => '2020-01-03',
            'biaya_distribusi' => '10000000',
            'indeks_lokasi' => '1,1',
            'indeks_jenis' => '0,9',
            'nama_penandatangan' => 'Afuza Imam Awed Alfaruq',
            'nip_penandatangan' => '199301062020121010',
        ]);
    }
}
