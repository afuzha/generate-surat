<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(SuratTableSeeder::class);
        $this->call(PerusahaanSeeder::class);
        $this->call(IndeksDistribusiSeeder::class);
        $this->call(DataSKRDSeeder::class);
        $this->call(DataRekomendasiSeeder::class);
        $this->call(MenimbangSeeder::class);
        $this->call(UntukSeeder::class);
    }
}
