<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use DB;

class PerusahaanSeeder extends Seeder
{
    public function run()
    {
        // $faker = Faker::create();
        // foreach (range(0, 5) as $i) {
        //     DB::table('perusahaan')->insert([
        //         'nama' => $faker->company,
        //         'alamat1' => $faker->address,
        //         'alamat2' => $faker->address
        //     ]);
        // }

        DB::table('perusahaan')->insert([
            [
                'nama' => 'PT Profesional Telekomunikasi Indonesia',
                'alamat1' => 'Menara BCA Lantai 55',
                'alamat2' => 'Jl. M. H. Thamrin No. 1, Jakarta Pusat'
            ],
            [
                'nama' => 'PT Dayamitra Telekomunikasi Head Office (MITRATEL HO)',
                'alamat1' => 'RT.6/RW.1, West Kuningan, Mampang Prapatan',
                'alamat2' => 'South Jakarta City, Jakarta 12710'
            ],
            [
                'nama' => 'PT Centratama Menara Indonesia',
                'alamat1' => 'TCC Batavia Tower One, Lt. 16-19',
                'alamat2' => 'Jl. Kyai Haji Mas Mansyur, Kav 126, Jakarta Pusat'
            ],
            [
                'nama' => 'PT Solu Sindo Kreasi Pratama',
                'alamat1' => 'Gedung TCl, Lt. 11, Kawasan Rasuna Epicentrum',
                'alamat2' => 'Jl. H. R. Rasuna, Said, Jakarta Selatan 12940'
            ],
            [
                'nama' => 'PT Tower Bersama',
                'alamat1' => 'Gedung TCl, Lt. 11, Kawasan Rasuna Epicentrum',
                'alamat2' => 'Jl. H. R. Rasuna, Said, Jakarta Selatan 12940'
            ],
            [
                'nama' => 'PT Indosat Tbk',
                'alamat1' => 'Jl. Medan Merdeka Barat No. 21',
                'alamat2' => 'Jakarta Pusat, Jakarta 10110'
            ],
        ]);
    }
}
