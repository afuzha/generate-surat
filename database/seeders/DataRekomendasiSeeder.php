<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class DataRekomendasiSeeder extends Seeder
{
    public function run()
    {
        DB::table('d_surat_rekomendasi')->insert([
            'id' => '1',
            'perusahaan_id' => '2',
            'nomor_surat_rekomendasi' => '555 / 200 / DKISP-I / II / 2021',
            'nomor_masuk' => 'DMT.0019/DV1/ROE-d1000000/II/2021',
            'tanggal_masuk' => '2021-02-02',
            'tentang' => 'Permohonan Rekomendasi',
            'nama_pemohon' => 'Afuza Imam Awed Alfaruq, S.Kom.',
            'alamat_menara' => 'Makarti Jaya, Rt/Rw 017/003, Desa Makarti Jaya',
            'nama_site' => 'MT_MEKARTI',
            'peruntukan_menara' => 'Menara Telekomunikais (BTS) Multi Operator',
            'koordinat_menara' => 'E 111.725690 ; S -02.460750',
            'tinggi_menara' => '52',
            'nama_penandatangan' => 'AFUZA IMAM AWED ALFARUQ, S.Kom.',
            'nip_penandatangan' => '19930106 202012 1 010'
        ]);
    }
}
