<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class MenimbangSeeder extends Seeder
{
    public function run()
    {
        DB::table('menimbang')->insert([
            [
                'surat_id' => '1',
                'huruf' => 'a.',
                'isi' => 'Undang-Undang RI No. 36 Tahun 1999 tentang Telekomunikasi'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'b.',
                'isi' => 'Undang-Undang RI No. 28 Tahun 2009 tentang Pajak dan Retribusi Daerah'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'c.',
                'isi' => 'Peraturan Pemerintah RI No. 52 Tahun 2000 tentang Penyelenggaraan Telekomunikasi'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'd.',
                'isi' => 'Permen Kominfo RI No.02/PER/M.KOMINFO/3/2008 tentang Pedoman Pembangunan dan Penggunaan Menara Bersama'
            ],
            [
                'surat_id' => '1',
                'huruf' => 'e.',
                'isi' => 'Peraturan Daerah Kabupaten Kotawaringin Barat No.16 Tahun 2018 tentang Retribusi Pengendalian Menara Telekomunikasi'
            ]
        ]);
    }
}
