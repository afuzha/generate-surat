<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IndeksDistribusiSkrdTable extends Migration
{
    public function up()
    {
        Schema::create('indeks_distribusi_skrd', function (Blueprint $table) {
            $table->id('id');
            $table->string('nama');
            $table->string('indeks');
        });
    }

    public function down()
    {
        Schema::dropIfExists('indeks_distribusi_skrd');
    }
}
