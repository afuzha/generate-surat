<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MenimbangTable extends Migration
{
    public function up()
    {
        Schema::create('menimbang', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_id');
            $table->string('huruf');
            $table->string('isi');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('menimbang');
    }
}
