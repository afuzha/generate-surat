<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UntukTable extends Migration
{
    public function up()
    {
        Schema::create('untuk', function (Blueprint $table) {
            $table->id();
            $table->integer('surat_id');
            $table->string('huruf');
            $table->string('isi');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('untuk');
    }
}
