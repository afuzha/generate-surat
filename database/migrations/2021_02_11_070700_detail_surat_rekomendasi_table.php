<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetailSuratRekomendasiTable extends Migration
{
    public function up()
    {
        Schema::create('d_surat_rekomendasi', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->string('perusahaan_id');
            $table->string('nomor_surat_rekomendasi');
            $table->string('nomor_masuk');
            $table->date('tanggal_masuk');
            $table->string('tentang');
            $table->string('nama_pemohon');
            $table->string('alamat_menara');
            $table->string('nama_site');
            $table->string('peruntukan_menara');
            $table->string('koordinat_menara');
            $table->string('tinggi_menara');
            $table->string('nama_penandatangan');
            $table->string('nip_penandatangan');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_surat_rekomendasi');
    }
}
