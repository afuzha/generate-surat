<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DetailSuratSkrdTable extends Migration
{
    public function up()
    {
        Schema::create('d_surat_skrd', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->string('perusahaan_id');
            $table->string('nomor_surat_skrd');
            $table->string('jenis_menara');
            $table->string('jumlah_menara');
            $table->string('posisi_menara');
            $table->string('jumlah_retribusi');
            $table->string('nama_site');
            $table->string('jarak_tempuh');
            $table->date('masa_retribusi_start');
            $table->date('masa_retribusi_end');
            $table->date('jatuh_tempo');
            $table->string('biaya_distribusi');
            $table->string('indeks_lokasi');
            $table->string('indeks_jenis');
            $table->string('nama_penandatangan');
            $table->string('nip_penandatangan');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('d_surat_skrd');
    }
}
